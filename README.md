# Contains all the neccesary services to run airflow
# Credit to Puckel for the base image and fork

# Maintained by Rob

1. Airflow
2. Flower
3. Postgres - (TO BE MIGRATED)
4. Redis - (TO BE MIGRATED)

# Airflow
1. webserver
2. scheduler
3. worker - (Managed Services)

# Ports
-   8793 - logs
-   8080 - web


