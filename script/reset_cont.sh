#!/usr/bin/env bash

#stop all containers:
docker kill $(docker ps -q)
#remove all containers
docker rm $(docker ps -a -q)
#remove all docker images
docker rmi $(docker images -q)
#remove all docker volumes
docker volume ls -qf dangling=true | xargs -r docker volume rm


# After editing the Dockerfile run THIS
docker-compose build

# Push to ecr
eval $(aws ecr get-login --region us-east-2 --no-include-email)
docker tag echelon:latest 404661103226.dkr.ecr.us-east-2.amazonaws.com/echelon:latest && \
docker push 404661103226.dkr.ecr.us-east-2.amazonaws.com/echelon:latest

# Run docker compose script
docker-compose -f docker-compose.yml up -d --force-recreate



#rebuild the dockerfile
docker build 




#!/usr/bin/env bash

eval $(ssh-agent -s)
sudo chmod 600 ~/.ssh/fdo-dev-bb
ssh-add ~/.ssh/fdo-dev-bb

# Add bb to known hosts (partially solveed with yes | )
#ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
if [ ! -n "$(grep "^bitbucket.org " ~/.ssh/known_hosts)" ];
    then ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts 2>/dev/null;
fi

cd msched-echelon
yes | git pull
git submodule update --recursive --remote

sudo mkdir -p /usr/local/airflow/afefs
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 \
    fs-44f1063d.efs.us-east-2.amazonaws.com:/ /usr/local/airflow/afefs

sudo systemctl start docker

docker build -t echelon .

eval $(aws ecr get-login --region us-east-2 --no-include-email)
docker tag echelon:latest 404661103226.dkr.ecr.us-east-2.amazonaws.com/echelon:latest && \
docker push 404661103226.dkr.ecr.us-east-2.amazonaws.com/echelon:latest

sleep 5

docker run --rm --name echelon-worker \
--runtime=nvidia \
-itd --privileged \
--volume /usr:/hostusr \
--volume /usr/local/airflow/afefs:/usr/local/airflow/afefs \
404661103226.dkr.ecr.us-west-2.amazonaws.com/echelon:latest

# Start the ecs agent
sudo docker run --name ecs-agent \
--detach=true \
--restart=on-failure:10 \
--volume=/var/run:/var/run \
--volume=/var/log/ecs/:/log \
--volume=/var/lib/ecs/data:/data \
--volume=/etc/ecs:/etc/ecs \
--net=host \
--env-file=/etc/ecs/ecs.config \
amazon/amazon-ecs-agent:latest
