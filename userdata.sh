#!/usr/bin/env bash

eval $(ssh-agent -s)
sudo chmod 600 ~/.ssh/fdo-dev-bb
ssh-add ~/.ssh/fdo-dev-bb

# Add bb to known hosts (partially solveed with yes | )
#ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
if [ ! -n "$(grep "^bitbucket.org " ~/.ssh/known_hosts)" ];
    then ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts 2>/dev/null;
fi

cd msched
yes | git pull
git submodule update --recursive --remote

sudo mkdir -p /usr/local/airflow/afefs
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 \
    fs-44f1063d.efs.us-east-2.amazonaws.com:/ /usr/local/airflow/afefs

sudo systemctl start docker
