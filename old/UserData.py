#!/usr/bin/env python

import httplib
import urllib2
import os
from boto.s3.connection import S3Connection
import boto


def get_http(base_url, endpoint):
    conn = httplib.HTTPConnection(base_url)
    conn.request("GET", endpoint)
    out = conn.getresponse()
    print out.status, out.reason
    return out.read()


# Write AWS Credentials to file
conn = httplib.HTTPConnection("www.python.org")
conn.request("GET", "/index.html")
aws_cred = conn.getresponse()
print aws_cred.status, aws_cred.reason
aws_cred_file = aws_cred.read()

"""
aws_cred_file = get_http("www.python.org", "/index.html")
"""

with open('~/.aws/credentials', 'w') as output:
    output.write(aws_cred_file)

# Figure out which kind of instance it is

conn = httplib.HTTPConnection("http://169.254.169.254/latest/meta-data/")
conn.request("GET", "/instance-id")
aws_cred = conn.getresponse()
print aws_cred.status, aws_cred.reason
aws_cred_file = aws_cred.read()


instanceid = urllib2.urlopen('http://169.254.169.254/latest/meta-data/instance-id').read()


conn = S3Connection()

ec2conn = boto.connect_ec2()
reservations = ec2conn.get_all_instances()
instances = [i for r in reservations for i in r.instances]

def make_tag_dict(ec2_object):
    """Given an tagable ec2_object, return dictionary of existing tags."""
    tag_dict = {}
    if ec2_object.tags is None: return tag_dict
    for tag in ec2_object.tags:
        tag_dict[tag['Key']] = tag['Value']
    return tag_dict



conn = ec2.connect_to_region(ec2_region)

reservations = conn.get_all_instances()
instances = [i for r in reservations for i in r.instances]

for instance in instances:
    if instance.__dict__['id'] == ec2_instance_id:
        print instance.__dict__['tags']['environment']







